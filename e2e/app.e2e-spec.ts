import { TeamAppPage } from './app.po';

describe('team-app App', function() {
  let page: TeamAppPage;

  beforeEach(() => {
    page = new TeamAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
